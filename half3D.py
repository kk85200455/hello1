from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

def detection(x,y,z, head = [17,15,0,16,18] ):
    x = np.array(x.split(","), dtype = "float32") 
    y = np.array(y.split(","), dtype = "float32")
    z = np.array(z.split(","), dtype = "float32")
    #z = z * - 1

    fig1 = plt.figure(figsize=(5,7))
    ax = plt.subplot(projection='3d')
    
    Rhalf=np.array([15,17,2,3,4,9])
    Lhalf=np.array([16,18,5,6,7,12])
    for i in range(len(Rhalf)):
        Rdex = Rhalf[i]
        if y[Rdex] == 0.0:
            y[Rdex] = y[Lhalf[i]]
        Ldex = Lhalf[i]
        if y[Ldex] == 0.0:
            y[Ldex] = y[Rhalf[i]]

    headx=[x[head[0]],x[head[1]],x[0],x[16],x[18]]
    heady=[y[17],y[15],y[0],y[16],y[18]]
    headz=[z[17],z[15],z[0],z[16],z[18]]
    handx=[x[4],x[3],x[2],x[1],x[5],x[5],x[6],x[7]]
    handy=[y[4],y[3],y[2],y[1],y[5],y[5],y[6],y[7]]
    handz=[z[4],z[3],z[2],z[1],z[5],z[5],z[6],z[7]]
    bodyx=[x[0],x[1],x[8]]
    bodyy=[y[0],y[1],y[8]]
    bodyz=[z[0],z[1],z[8]]
    legx=[x[9],x[8],x[12]]
    legy=[y[9],y[8],y[12]]
    legz=[z[9],z[8],z[12]]

    ax.plot(headx,heady,headz,color='r') #頭
    ax.plot(handx,handy,handz,color='b') #手
    ax.plot(bodyx,bodyy,bodyz,color='m') #身體
    ax.plot(legx,legy,legz,color='c') #腿

    ax.scatter(headx,heady,headz,color='k')
    ax.scatter(handx,handy,handz,color='k')
    ax.scatter(bodyx,bodyy,bodyz,color='k')
    ax.scatter(legx,legy,legz,color='k')

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.set_ylim(100.0, 2000.0)
    ax.set_zlim(2000.0, 650.0)

    plt.draw()
    plt.show()
