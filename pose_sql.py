import sqlite3
import time
import tkinter.messagebox as box

class SQL:
    def __init__(self):
        self.conn = sqlite3.connect("pose.db")
        self.create = self.conn.cursor()
        #要修改的變數名稱

    def create_basic(self):
        self.create.execute("CREATE TABLE IF NOT EXISTS 病患基本資料表" + 
        "('序號' INTEGER PRIMARY KEY,"+
        "'身分證字號' TEXT type UNIQUE,"+
        "'姓名' TEXT NOT NULL,"+
        "'性別' TEXT NOT NULL,"+
        "'身高' FLOAT NOT NULL,"+
        "'體重' INT NOT NULL"+
        ");")
        self.conn.commit()

    def create_problem(self):
        self.create.execute("CREATE TABLE IF NOT EXISTS 檢診紀錄表"
        "('序號' INTEGER PRIMARY KEY AUTOINCREMENT,"+
        "'檢驗序號' INT type UNIQUE,"+
        "'身分證字號' TEXT  NOT NULL,"+
        "'正身x' TEXT  NOT NULL," +
        "'正身y' TEXT  NOT NULL," +
        "'側身x' TEXT  NOT NULL," +
        "'絕對頭水平' TEXT  NOT NULL,"+
        "'相對頭水平' TEXT  NOT NULL,"+
        "'絕對肩水平' TEXT  NOT NULL,"+
        "'絕對脊椎' TEXT NOT NULL,"+
        "'相對脊椎' TEXT  NOT NULL,"+
        "'絕對頸椎' TEXT  NOT NULL,"+
        "'相對頸椎' TEXT  NOT NULL,"+
        "'時間' TEXT  NOT NULL,"+
        "'醫生建議' TEXT  NOT NULL,"+
        "'問題' TEXT  NOT NULL,"+
        "'正面偵測圖檔案位置' TEXT  NOT NULL,"+
        "'側面偵測圖檔案位置' TEXT  NOT NULL,"+
        "FOREIGN KEY (身分證字號) REFERENCES 病患基本資料表 (身分證字號)"+
        ");")
        self.conn.commit()

    def insert_basic(self,bas_data):
        if (len(list(self.check_exits(bas_data[0]))) == 0):
            self.create.execute("insert into 病患基本資料表(身分證字號,姓名,性別,身高,體重)" + 
            "\n VALUES("+"?,?,?,?,?)",bas_data)
            self.conn.commit()
            box.showinfo("","資料新增完成")
        else:
            box.showerror("錯誤","已存在身分證字號")


    def insert_problem(self, pro_data):
        t = time.time()
        ti = time.localtime(t)
        s = str(time.mktime(ti))
        T = str(time.strftime("%Y/%m/%d %H:%M:%S",time.localtime()))
        self.create.execute("insert into 檢診紀錄表 (檢驗序號,身分證字號,正身x,正身y,側身x,絕對頭水平,相對頭水平,絕對肩水平,絕對脊椎,相對脊椎,絕對頸椎,相對頸椎,時間,醫生建議,問題,正面偵測圖檔案位置,側面偵測圖檔案位置)\n " + 
        "VALUES("+s + ",?,?,?,?,?,?,?,?,?,?,?,'" +T +"',?,?,?,?)",pro_data)
 
        self.conn.commit()


    def check_exits(self, patient_id_number):
        E =self.create.execute("select * from 病患基本資料表 where 身分證字號 ='"+str(patient_id_number)+"'")
        return E
    
    def select_basic(self, patient_id_number):
        if (patient_id_number != ""):
            E =self.create.execute("select * from 病患基本資料表 where 身分證字號 ='"+str(patient_id_number)+"'")
        return E
        
    
    def select_problem(self,patient_id_number):
        problem_data = self.create.execute("select * from 檢診紀錄表 where 身分證字號 ='"+str(patient_id_number)+"'")

        return problem_data
    def select_problem_time(self, history_time):
        
        problem_data = self.create.execute("select * from 檢診紀錄表 where 時間 ='"+str(history_time)+"'")

        return problem_data
    

    def update(self, patient_basic_data):
        try:
            self.create.execute("UPDATE 病患基本資料表 set 姓名 = '"+str(patient_basic_data[1])+"',性別 = '"+ str(patient_basic_data[2])+"',身高 = "+str(patient_basic_data[3])+",體重 = "\
                                +str(patient_basic_data[4])+"\n  where 身分證字號 = '"+str(patient_basic_data[0])+"';")
        except:
            box.showerror("錯誤","輸入型態不符合")
        self.conn.commit()

    def delete_basic(self,patient_id_number):
        self.create.execute("delete from 檢診紀錄表 \n where 身分證字號 = '"+patient_id_number+"';")
        self.create.execute("delete from 病患基本資料表 \n where 身分證字號 = '"+patient_id_number+"';")
        self.conn.commit()
    #def delete_all_problem(self, patient_id_number):
    def delete_problem(self,history_time):
        self.create.execute("delete from 檢診紀錄表 \n where 時間 = '"+history_time+"';")
        self.conn.commit()

sql = SQL()
sql.create_basic()
sql.create_problem()
