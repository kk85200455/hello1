import uvicorn
from fastapi import FastAPI, HTTPException, UploadFile, File
from fastapi.responses import FileResponse
import shutil
import os
from datetime import datetime
import cv2
from tfposeProcess import TfProcess
import time

app = FastAPI()

tf = TfProcess()

if not os.path.exists("./picture"):
    os.makedirs("./picture")
'''
if not os.path.exists("./result"):
    os.makedirs("./result")
'''

@app.post("/upload")
async def uploadPicture(file: UploadFile = File(...)):
    timestamp = int(datetime.now().timestamp())
    with open(f"./picture/{timestamp}.jpg", "wb") as buf:
        shutil.copyfileobj(file.file, buf)

    img = cv2.imread(f"./picture/{timestamp}.jpg")
    resImg, kp = tf.run(img)
    cv2.imwrite(f"./result/{timestamp}.jpg", resImg)

    return {"kp": kp.tolist(), "timestamp": timestamp}


@app.post("/takePicture")
async def takePicture():
    timestamp = int(datetime.now().timestamp())
    try:
        os.system("fswebcam -S 20 --set brightness=50% --set contrast=40%  -F 10 -r 1920x1080 --no-banner webcam1.jpg")
        img = cv2.imread('webcam1.jpg')
        print("Done")
    except Exception as e:
        print(e)
        raise HTTPException(500, "Error when taking picture")
    img = cv2.flip(cv2.transpose(img), 0)
    
    cv2.imwrite(f"./picture/{timestamp}.jpg", img)
    os.remove("./webcam1.jpg")
    kp = tf.run(img)
    return {"kp": kp.tolist(), "timestamp": timestamp}
    #return {"timestamp": timestamp}


@app.get("/picture/{timestamp}")
async def getPicture(timestamp=int):
    if os.path.exists(f"./picture/{timestamp}.jpg"):
        return FileResponse(f"./picture/{timestamp}.jpg")
    return HTTPException(404, "File Not Found")


@app.get("/result/{timestamp}")
async def getPicture(timestamp=int):
    if os.path.exists(f"./result/{timestamp}.jpg"):
        return FileResponse(f"./result/{timestamp}.jpg")
    return HTTPException(404, "File Not Found")

@app.post("/picture/{timestamp}")
async def remove(timestamp=int):
    os.remove(f"./picture/{timestamp}.jpg")


if __name__ == "__main__":
    uvicorn.run(app, host = "0.0.0.0",port=8927)

