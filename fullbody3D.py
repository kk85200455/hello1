from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

def detection(x,y,z ):
    
    x = np.array(x.split(","), dtype = "float32") 
    y = np.array(y.split(","), dtype = "float32")
    z = np.array(z.split(","), dtype = "float32")
    
    #z = z * - 1
    
    Rhalf=np.array([15,17,2,3,4,9,23,22,11,24])
    Lhalf=np.array([16,18,5,6,7,12,21,14,19,20])
    for r,l in zip(Rhalf,Lhalf):

        if y[r] == 0.0:
            y[r] = y[l]

        if y[l] == 0.0:
            y[l] = y[r]

    headx=[x[17],x[15],x[0],x[16],x[18]]
    heady=[y[17],y[15],y[0],y[16],y[18]]
    headz=[z[17],z[15],z[0],z[16],z[18]]
    handx=[x[4],x[3],x[2],x[1],x[5],x[5],x[6],x[7]]
    handy=[y[4],y[3],y[2],y[1],y[5],y[5],y[6],y[7]]
    handz=[z[4],z[3],z[2],z[1],z[5],z[5],z[6],z[7]]
    bodyx=[x[0],x[1],x[8]]
    bodyy=[y[0],y[1],y[8]]
    bodyz=[z[0],z[1],z[8]]
    legx=[x[10],x[11],x[9],x[8],x[12],x[13],x[14]]
    legy=[y[10],y[11],y[9],y[8],y[12],y[13],y[14]]
    legz=[z[10],z[11],z[9],z[8],z[12],z[13],z[14]]
    feetx=[x[23],x[22],x[11],x[24],x[21],x[14],x[19],x[20]]
    feety=[y[23],y[22],y[11],y[24],y[21],y[14],y[19],y[20]]
    feetz=[z[23],z[22],z[11],z[24],z[21],z[14],z[19],z[20]]

    fig1 = plt.figure(figsize=(5,7))
    ax = plt.subplot(projection='3d')    
    ax.plot(headx,heady,headz,color='r') #頭
    ax.plot(handx,handy,handz,color='b') #手
    ax.plot(bodyx,bodyy,bodyz,color='m') #身體
    ax.plot(legx,legy,legz,color='c') #腿
    ax.plot(feetx,feety,feetz,color='y') #腳
  

    ax.scatter(x,y,z,color='k')

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    
    ax.set_ylim(100.0, 2000.0)
    ax.set_zlim(2000.0, 650.0)
    
    plt.draw()
    plt.show()

if __name__ == "__main__":
    x = np.array([1489.893,1489.5004,1209.3701,1153.7028,1031.0543,1780.5664,1791.7397,1837.2336,1489.5101,1321.773,1388.8472,1444.906,1657.389,1635.1381,1635.0021,1444.9021,1567.7701,1366.3026,1657.6376,1567.693,1657.3828,1635.2069,1411.1708,1355.2441,1467.4784]) 
    y = np.array([1467.3837,1735.4591,1658.1152,1466.9174,1186.9526,1758.3866,1926.611,2082.6619,1702.3063,1657.6594,1691.1241,1769.6547,1736.0645,1746.9927,1792.0958,0.0,1489.8975,0.0,1658.2179,1478.5519,1545.7789,1837.0675,1568.1138,1623.8774,1847.732])
    z = np.array([571.94995,985.74304,985.8327,1456.1609,1836.9396,974.57794,1466.7094,1836.836,1881.3903,1870.1573,2519.576,3158.0222,1881.6147,2553.085,3179.905,504.67502,504.48038,582.73346,582.79443,3370.363,3359.071,3225.227,3348.503,3325.8826,3191.3499])
    detection(x,y,z)