import numpy as np

def arccos_angle(p1, p2):
    distance = (p1[0] - p2[0])**2 + (p1[1] - p2[1])**2
    a = np.sqrt([distance])
    theta = np.sqrt([(p1[0] - p2[0])**2]) / a
    print(a,theta)
    degree = np.arccos(theta) * 180 / np.pi
    return degree #np.arctan( (p2[1] - p1[1]) / (p2[0] - p1[0]) ) * 180 / np.pi

def point_to_vector(p1 , p2):
    vector = p2 - p1
    return vector

def est_distance(v1):
    dist = np.sqrt([v1[0]**2 + v1[1]**2])
    return dist

def est_angle(v1, v2):
    v3 = v2 - v1
    a = est_distance(v1)
    b = est_distance(v2)
    c = est_distance(v3)
    cos_value = (a**2 + b**2 -c**2)/(2*a*b)
    degree = np.arccos(cos_value) * 180 / np.pi
    return degree

if __name__ == "__main__":
    
    v1 = point_to_vector(np.array([191.02281, 214.56822]),  np.array([198.1386, 110.254715]))
    v2 = point_to_vector(np.array([201.2525, 72.62311]),  np.array([191.02281, 214.56822]))
    arccos_angle(np.array([1,1]),np.array([2,2]))
    print(arccos_angle(np.array([1,1]),np.array([2,2])))
    print(v1,v2)
    theta = est_angle(v1,v2)
    print(theta)
