from tkinter import *
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
import os, sys
import cv2
import time
#引用openpose路徑
def Openpose_Process(img, img1, img_ch, img1_ch, image_canvas, image1_canvas):
    import pyopenpose as op
    op_img, op_img1 = img, img1
    params = {}
    params['model_folder'] = '/home/lab8927/local/openpose/models'
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()
    datum = op.Datum()
    t = time.time()
    ti = time.localtime(t)
    s = str(time.mktime(ti))
    if (img.all() != None):
        datum.cvInputData = op_img
        opWrapper.emplaceAndPop([datum])
        op_img = datum.cvOutputData
        pose_key = datum.poseKeypoints
        img_ch = True
    if (img1.all() != None):
        datum.cvInputData = op_img1
        opWrapper.emplaceAndPop([datum])
        op_img1 = datum.cvOutputData
        pose1_key = datum.poseKeypoints
        img1_ch = True
    
    return pose_key, pose1_key, op_img, op_img1, img_ch, img1_ch