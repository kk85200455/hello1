import base64
from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import font as tkFont
from tkinter.ttk import Separator,Style#分隔線
from tkinter import messagebox
from PIL import Image, ImageTk
import os, sys
import cv2
from tkinter import filedialog
import numpy as np
import math
import pose_sql
import paramiko
import time
import fullbody3D
import AngelEstimation as math_estimation
import pandas as pd
import io
import json
import requests

#宣告關鍵點編號
Nose, Neck, RShoulder, LShoulder, MidHip, Reye, LEye, LEar = 0, 1, 2, 5, 8, 15, 16, 18
report_column = [13, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 14]
human_line = [(17,15), (15,0), (0,16), (16,18), (0,1), (1,2), (2,3), (3,4), (1,5), (5,6), (6,7), (1,8), (8,9), (9,10), (10,11), (8,12), (12,13), (13,14)]
class frame_label:
    def result_label_frame(frame, position_text, normal_text, result_text, position_x, position_y, normal_x, normal_y, result_x, result_y):
        block_label = tk.Label(frame, bg = '#7c97c4')
        block_label.place(x = position_x, y = position_y, height = 120, width = 300)
    
        title_label = tk.Label(frame, text = position_text, bg = '#7c97c4', fg="white", justify="center", font="微軟正黑體 24 bold", width=12)
        title_label.place(x = position_x, y = position_y, height = 50, width = 300)
    
        normal_label = tk.Label(frame, text = normal_text, bg = '#39537e', fg="white", justify="center", font="微軟正黑體 15 bold", width=12)
        normal_label.place(x = normal_x, y = normal_y, height = 70, width = 148)
    
        re_label = tk.Label(frame, text = result_text, bg = '#39537e', fg="white", justify="center", font="微軟正黑體 15 bold", width=12)
        re_label.place(x = result_x, y = result_y, height = 70, width = 148)
    def result_label_frame1(frame, position_text, normal_text, position_x, position_y, normal_x, normal_y):
        block_label = tk.Label(frame, bg = '#7c97c4')
        block_label.place(x = position_x, y = position_y, height = 120, width = 300)
    
        title_label = tk.Label(frame, text = position_text, bg = '#7c97c4', fg="white", justify="center", font="微軟正黑體 24 bold", width=12)
        title_label.place(x = position_x, y = position_y, height = 50, width = 300)
    
        normal_label = tk.Label(frame, text = normal_text, bg = '#39537e', fg="white", justify="center", font="微軟正黑體 15 bold", width=12)
        normal_label.place(x = normal_x, y = normal_y, height = 70, width = 300)

class App:    #基本資料表 姓名  檢測紀錄表 數據6筆 圖片兩張  問題 建議
    def destroy(self):
        self.new_window.destroy()
##更改
    def history_result(self):
        all_problem_data = []
        for item in self.tree2.selection():
            item_text = self.tree2.item(item,"values")
            self.history_time = item_text[0]
        problem_data = sql.select_problem_time(self.history_time)
        for data in problem_data:
            all_problem_data.append(data)
        new_window = tk.Toplevel()
        new_window = new_window
        new_window.geometry('1200x800')
        new_window.configure(bg = '#222a35')
        new_window.resizable(width=0, height=0)
        #排版
        #顯示時間
        time_block = tk.Frame(new_window, bg = '#222a35')
        time_block.place(x = 0, y = 0, height = 50, width = 550)
        
        time_label = tk.Label(time_block, bg = '#222a35', text = self.history_time, fg="white")
        time_label.place(x = 5, y = 5, height = 50, width = 550)
        #圖片排版
        photo_frame = tk.Frame(new_window ,bg = '#222a35')
        photo_frame.place(x = 0, y = 50, height = 650, width = 550)
        #檢測結果、按鈕排版
        top_title_frame = tk.Frame(new_window, bg = '#222a35')
        top_title_frame.place(x = 550, y = 0, height = 100, width = 450)

        top_button_frame = tk.Frame(new_window, bg = '#222a35')
        top_button_frame.place(x = 1000, y = 0, height = 100, width = 200)
        
        button_font = tkFont.Font(family='微軟正黑體', size=8, weight='bold')
        
        #檢測數據顯示區塊
        result_frame = tk.Frame(new_window, bg = '#2f466e')
        result_frame.place(x = 550, y = 100, height= 450, width = 750)
        #問題改善區塊
        question_frame = tk.Frame(new_window, bg = '#222a35')
        question_frame.place(x = 0, y = 700, height = 90, width = 550)
        #醫生評論區  width = window_w - image_w  ==> 1200 - 550 = 650, height = window_h - result_h - tot_title_h ==> 800 - 115 - 450 
        doctor_frame = tk.Frame(new_window, bg = '#222a35')
        doctor_frame.place(x = 550, y = 550, height = 240, width = 640)
        #------- 排版區塊分割線 ----------
        #image canvas 放置
        image_canvas = tk.Canvas(photo_frame, bg = '#333f50',height = 315, width = 530)
        image_canvas.place(x = 10, y = 5)

        image1_canvas = tk.Canvas(photo_frame, bg = '#333f50',height = 315, width = 530)
        image1_canvas.place(x = 10, y = 330)
        img = cv2.cvtColor(cv2.imread(all_problem_data[0][16]), cv2.COLOR_BGR2RGB)
        #img = cv2.imread(all_problem_data[0][16])
        img_re = cv2.resize(img, (530, 315))
        img_show = ImageTk.PhotoImage(Image.fromarray(img_re))
        image_canvas.create_image(0,0,anchor = NW, image = img_show)
        image_canvas.img = img_show

        img1 = cv2.cvtColor(cv2.imread(all_problem_data[0][17]), cv2.COLOR_BGR2RGB)
        #img1 = cv2.imread(all_problem_data[0][17])
        img_re = cv2.resize(img1, (530, 315))
        img_show = ImageTk.PhotoImage(Image.fromarray(img_re))
        image1_canvas.create_image(0,0,anchor = NW, image = img_show)
        image1_canvas.img = img_show

        #檢測結果、顯示頂端
        button_font = tkFont.Font(family='微軟正黑體', size=8, weight='bold')
        button_3d = tk.Button(top_button_frame, width = 13, height = 2, text = "3D模型", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                font=self.button_font, command = lambda : half3D.detection(all_problem_data[0][3], all_problem_data[0][4],all_problem_data[0][5]))
        button_3d.place(x = 5, y = 2)  #100 8
        title_label = tk.Label(top_title_frame, text="",# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字体和字体大小
                               fg="#FFFFFF"
                              )
        title_label.grid(row = 0, column = 0, rowspan = 3)
        '''
        title_label1 = tk.Label(top_title_frame, text='白色：正常角度',# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 16, 'bold'),     # 字体和字体大小
                               fg="#FFFFFF"
                              )
        title_label1.grid(row = 0, column = 1,sticky=N + W, padx = 5)


        title_label2 = tk.Label(top_title_frame, text='綠色：實際角度在合理範圍內',# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 16, 'bold'),# 字体和字体大小
                               fg="#00FF00"
                              )
        title_label2.grid(row = 1, column = 1,sticky=N + W, padx = 5)

        title_label3 = tk.Label(top_title_frame, text='紅色：實際角度超標',# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 16, 'bold'),     # 字体和字体大小
                               fg="#FF0000",
                              )
        title_label3.grid(row = 2, column = 1,sticky=N + W, padx = 5)
        '''
        #檢測結果顯示  title height = 50 w = 200  re,ma h = 50 , w = 80
        frame_label.result_label_frame(result_frame, "頭水平", "相對水平軸" + str(all_problem_data[0][6]) + "度", "相對肩偏移" + str(all_problem_data[0][7]) + "度", 5, 10, 5, 60, 157, 60)
        frame_label.result_label_frame1(result_frame, "肩膀", "相對水平軸" + str(all_problem_data[0][8]) + "度", 340, 10, 340, 60)
        frame_label.result_label_frame(result_frame, "脊椎", "左傾" + str(all_problem_data[0][9]) + "單位", "脊椎頭" + "\n" + "連線" + str(all_problem_data[0][10]) + "度", 340, 155, 340, 205, 492, 205)
        frame_label.result_label_frame(result_frame, "頸椎", "前移" + str(all_problem_data[0][11]) + "單位", "前傾" + str(all_problem_data[0][12]) + "度", 5, 155, 5, 205, 157, 205)
        #下方評論區
        text_question = tk.Text(question_frame, bg = '#333e50', fg = '#FFFFFF')
        text_question.place(x = 5, y = 5)
        text_question.tag_configure("center", justify='center')
        text_question.insert('end', all_problem_data[0][15]) 

        text_doctor = tk.Text(doctor_frame,bg = '#333e50', fg = '#FFFFFF')
        text_doctor.place(x = 5, y = 5, width = 650)
        text_doctor.tag_configure("center", justify='center')
        text_doctor.insert('end', all_problem_data[0][14])
        
        text_doctor.configure(state='disabled')
        text_question.configure(state='disabled')
    def OnDoubleClick(self, event):
        self.patient_data = []
        for item in self.tree.selection():
            item_text = self.tree.item(item,"values")
            self.ID = item_text[0]
            self.patient_name = item_text[1]
        self.patient_data.append(self.ID)
        self.title_label = tk.Label(self.top_title_frame, text=self.patient_name,# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字体和字体大小
                               fg="#FFFFFF"
                              )
        self.title_label.grid(row = 0, column = 0, rowspan = 3)
        self.tabControl.add(self.tab3, state = "normal")
        self.tabControl.select(self.tab3)
        
    def OneClick(self, event):
        self.all_history_button['state'] = NORMAL
        self.update_patient_button['state'] = NORMAL
        self.delete_button['state'] = NORMAL
    def delete_OneClick(self, event):
        self.delete_button['state'] = NORMAL
    #更改
    def show_result(self, event):
        self.history_result()
    def add_patient(self, tab1):
        #------------------------------病患資料新增版面-------------------------------------
        self.IDnumber_label = tk.Label(tab1, text='身份證字號：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        self.IDnumber_label.place(x = 300, y = 100)
        self.name_label = tk.Label(tab1, text='姓名：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        self.name_label.place(x = 410, y = 200)
        self.gender_label = tk.Label(tab1, text='性別：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        self.gender_label.place(x = 410, y = 300)
        self.height_label = tk.Label(tab1, text='身高：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        self.height_label.place(x = 410, y = 400)
        self.weight_label = tk.Label(tab1, text='體重：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        self.weight_label.place(x = 410, y = 500)

        self.IDnumber_entry = Entry(tab1, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.IDnumber_entry.place(x = 550, y = 100, width=350, height=50)

        self.name_entry = Entry(tab1, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.name_entry.place(x = 550, y = 200, width=350, height=50)

        self.combo = ttk.Combobox(tab1, value = ["男","女"], font=('微軟正黑體', 25, 'bold'))
        self.combo.place(x = 550, y = 300, width=350, height=50)
        # self.combo.current(0)預設值
        self.combo['state'] = 'readonly'

        self.height_entry = Entry(tab1, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.height_entry.place(x = 550, y = 400, width=350, height=50)

        self.weight_entry = Entry(tab1, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.weight_entry.place(x = 550, y = 500, width=350, height=50)

        self.new_button = tk.Button(tab1,text='新增資料',font=('微軟正黑體', 20, 'bold'), bg = '#8397b0', fg="#FFFFFF" ,command = self.add_data)
        self.new_button.place(x = 650, y = 600)
    def patient_history(self, tab2):
        #------------------------病患基本資料---------------------------------
        self.id_label = tk.Label(tab2, text='身份證字號：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        self.id_label.place(x = 0, y = 5)

        self.id_entry = Entry(tab2, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.id_entry.place(x = 230, y = 10, width=250, height=50)

        self.all_history_button = tk.Button(tab2,text='歷史報表',font=('微軟正黑體', 16, 'bold'), bg = '#8397b0', fg="#FFFFFF", command = self.history_report)
        self.all_history_button.place(x = 600, y = 10)
        self.all_history_button['state'] = DISABLED

        self.update_patient_button = tk.Button(tab2,text='修改資料',font=('微軟正黑體', 16, 'bold'), bg = '#8397b0', fg="#FFFFFF", command = self.modify_data)
        self.update_patient_button.place(x = 750, y = 10)
        self.update_patient_button['state'] = DISABLED
        
        self.inquire_button = tk.Button(tab2,text='查詢資料',font=('微軟正黑體', 16, 'bold'), bg = '#8397b0', fg="#FFFFFF", command = self.search_data)
        self.inquire_button.place(x = 900, y = 10)
        
        self.delete_button = tk.Button(tab2,text='刪除資料',font=('微軟正黑體', 16, 'bold'), bg = '#8397b0', fg="#FFFFFF", command = self.delete_data)
        self.delete_button.place(x = 1050, y = 10)
        self.delete_button['state'] = DISABLED
        #------------------------樹狀表格數據---------------------------------
        self.tree = ttk.Treeview(tab2, columns=['1','2','3','4','5'], show='headings')#建立treeview
        self.tree.bind("<Button-1>",self.OneClick)#事件
        self.tree.column('1', width=100, anchor='center')
        self.tree.column('2', width=100, anchor='center')
        self.tree.column('3', width=100, anchor='center')
        self.tree.column('4', width=100, anchor='center')
        self.tree.column('5', width=100, anchor='center')
        self.tree.heading('1', text='身份證字號')#建立圖標欄位
        self.tree.heading('2', text='姓名')
        self.tree.heading('3', text='性別')
        self.tree.heading('4', text='身高')
        self.tree.heading('5', text='體重')
        self.tree.place(x = 1, y = 80, width=1194, height=50)
        self.tree.bind("<Double-1>",self.OnDoubleClick)#事件

        self.tree2 = ttk.Treeview(tab2, columns=['1','2'], show='headings')#建立treeview
        self.yscrollbar = Scrollbar(tab2)#terrview綁定捲軸
        self.yscrollbar.place(x = 1173, y = 160, width=20, height=658)
        self.yscrollbar.config(command = self.tree2.yview)#捲軸設定
        self.tree2.configure(yscrollcommand = self.yscrollbar.set)
        self.tree2.bind("<Double-1>",self.history_result)#事件
        
        self.tree2.column('1', width=100, anchor='center')
        self.tree2.column('2', width=100, anchor='center')
        self.tree2.heading('1', text='時間')
        self.tree2.heading('2', text='醫生建議')
        
        self.tree2.place(x = 1, y = 135, width=1194, height=685)
        self.tree2.bind("<Double-1>",self.show_result)
    def detection_page(self, tab3):
        #排版
        #讀取、即時按鈕排版
        self.function_button = tk.Frame(tab3, bg = '#222a35')
        self.function_button.place(x = 0, y = 0, height = 130, width = 550)
        #圖片排版
        self.photo_frame = tk.Frame(tab3 ,bg = '#222a35')
        self.photo_frame.place(x = 0, y = 80, height = 700, width = 550)
        #檢測結果、按鈕排版
        self.top_title_frame = tk.Frame(tab3, bg = '#222a35')
        self.top_title_frame.place(x = 550, y = 0, height = 120, width = 320)

        self.top_button_frame = tk.Frame(tab3, bg = '#222a35')
        self.top_button_frame.place(x = 880, y = 0, height = 120, width = 320)
        #檢測數據顯示區塊
        self.result_frame = tk.Frame(tab3, bg = '#2f466e')
        self.result_frame.place(x = 550, y = 120, height= 450, width = 750)
        #問題改善區塊
        self.question_frame = tk.Frame(tab3, bg = '#222a35')
        self.question_frame.place(x = 0, y = 750, height = 90, width = 550)
        #醫生評論區  width = window_w - image_w  ==> 1200 - 550 = 650, height = window_h - result_h - tot_title_h ==> 800 - 115 - 450 
        self.doctor_frame = tk.Frame(tab3, bg = '#222a35')
        self.doctor_frame.place(x = 550, y = 570, height = 280, width = 640)
        
        #------- 排版區塊分割線 ----------
        #image canvas 放置
        self.image_canvas = tk.Canvas(self.photo_frame, bg = '#333f50',height = 315, width = 530)
        self.image_canvas.place(x = 10, y = 25)

        self.image1_canvas = tk.Canvas(self.photo_frame, bg = '#333f50',height = 315, width = 530)
        self.image1_canvas.place(x = 10, y = 350)
        # 顯示圖片、影片、即時影像按鈕
        self.button_font = tkFont.Font(family='微軟正黑體', size=8, weight='bold')
        self.image1_button = tk.Button(self.function_button, width = 10, height = 1, text = "圖片1", bg = '#8397b0', border = 0, fg = '#FFFFFF' ,\
                          font=self.button_font, command = lambda : self.image_button_funtion(1))
        self.image1_button.place(x = 5, y = 3)

        self.image2_button = tk.Button(self.function_button, width = 10, height = 1, text = "圖片2", bg = '#8397b0', border = 0, fg = '#FFFFFF' ,\
                                  font=self.button_font, command = lambda : self.image_button_funtion(2))
        self.image2_button.place(x = 5, y = 45)

        self.picture1_button = tk.Button(self.function_button, width = 10, height = 1, text = "take a picture", bg = '#8397b0', border = 0, fg = '#FFFFFF' ,\
                                  font=self.button_font, command = lambda : self.postData(1))
        self.picture1_button.place(x = 185, y = 3)

        self.picture2_button = tk.Button(self.function_button, width = 10, height = 1, text = "take a picture", bg = '#8397b0', border = 0, fg = '#FFFFFF' ,\
                                  font=self.button_font, command = lambda : self.postData(2))
        self.picture2_button.place(x = 185, y = 45)

        self.video1_button = tk.Button(self.function_button, width = 10, height = 1, text = "影片1", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                  font=self.button_font, command = lambda : self.video_path(1))
        self.video1_button.place(x = 95, y = 3)

        self.video2_button = tk.Button(self.function_button, width = 10, height = 1, text = "影片2", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                  font=self.button_font, command = lambda : self.video_path(2))
        self.video2_button.place(x = 95, y = 45)

        #self.real_time1_button = tk.Button(self.function_button, width = 10, height = 1, text = "即時影像1", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                      #font=self.button_font, command = lambda : self.real_time_button(), state = "disabled")
        #self.real_time1_button.place(x = 185, y = 3)

        self.stat1_button = tk.Button(self.function_button, width = 10, height = 1, text = "即時開始1", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                 font=self.button_font, state = DISABLED, command = lambda : self.start_real_time(1))
        self.stat1_button.place(x = 280, y = 3)

        self.stat2_button = tk.Button(self.function_button, width = 10, height = 1, text = "即時開始2", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                 font=self.button_font, state = DISABLED, command = lambda : self.start_real_time(2))
        self.stat2_button.place(x = 280, y = 45)

        self.stop1_button = tk.Button(self.function_button, width = 10, height = 1, text = "即時停止1", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                 font=self.button_font, state = DISABLED, command = lambda : self.stop_real_time(1))
        self.stop1_button.place(x = 370, y = 3)

        self.stop2_button = tk.Button(self.function_button, width = 10, height = 1, text = "即時停止2", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                font=self.button_font, state = DISABLED, command = lambda : self.stop_real_time(2))
        self.stop2_button.place(x = 370, y = 45)
        '''
        self.screenshot1_button = tk.Button(self.function_button, width = 10, height = 1, text = "圖片存檔1", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                       font=self.button_font, state = DISABLED, command = lambda : self.screenshot(1))
        self.screenshot1_button.place(x = 460, y = 3)

        self.screenshot2_button = tk.Button(self.function_button, width = 10, height = 1, text = "圖片存檔2", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                               font=self.button_font, state = DISABLED, command = lambda : self.screenshot(2))
        self.screenshot2_button.place(x = 460, y = 45)
        '''
        #檢測結果、顯示頂端
        self.title_label = tk.Label(self.top_title_frame, text="",# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字体和字体大小
                               fg="#FFFFFF"
                              )
        self.title_label.grid(row = 0, column = 0, rowspan = 3)
        '''
        self.title_label1 = tk.Label(self.top_title_frame, text='白色：正常角度',# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 16, 'bold'),     # 字体和字体大小
                               fg="#FFFFFF"
                              )
        self.title_label1.grid(row = 0, column = 1,sticky=N + W, padx = 5)


        self.title_label2 = tk.Label(self.top_title_frame, text='綠色：角度在合理範圍',# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 16, 'bold'),# 字体和字体大小
                               fg="#00FF00"
                              )
        self.title_label2.grid(row = 1, column = 1,sticky=N + W, padx = 5)

        self.title_label3 = tk.Label(self.top_title_frame, text='紅色：實際角度超標',# 标签的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 16, 'bold'),     # 字体和字体大小
                               fg="#FF0000",
                              )
        self.title_label3.grid(row = 2, column = 1,sticky=N + W, padx = 5)
        '''
        #開始檢測與關閉按鈕
        self.button_font = tkFont.Font(family='微軟正黑體', size=15, weight='bold')
        
        self.save_button =tk.Button(self.top_button_frame, width = 5, height = 1, text = "儲存", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                font=self.button_font, state = DISABLED, command = lambda : self.save_result()) 
        self.save_button.place(x = 174, y = 75)  #100 8
        
        self.stat_button = tk.Button(self.top_button_frame, width = 11, height = 2, text = "開始檢測", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                font=self.button_font, state=DISABLED, command = lambda : self.start_test())
        self.stat_button.place(x = 174, y = 2)  #100 55
        
        self.end_button = tk.Button(self.top_button_frame, width = 5, height = 1, text = "結束", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                               font=self.button_font, command = lambda : self.button_end()) 
        self.end_button.place(x = 245, y = 75) # 3 55
        #3D
        self.model_button =tk.Button(self.top_button_frame, width = 13, height = 2, text = "3D模型", bg = '#8397b0', border = 0, fg = '#FFFFFF',\
                                font=self.button_font, state = DISABLED, command = self.show_3D) 
        self.model_button.place(x = 5, y = 2)  #100 8

        #檢測結果顯示  title height = 50 w = 200  re,ma h = 50 , w = 80
        frame_label.result_label_frame(self.result_frame, "頭水平", "檢測結果", "檢測角度", 5, 10, 5, 60, 157, 60)
        frame_label.result_label_frame1(self.result_frame, "肩膀", "檢測結果", 340, 10, 340, 60)
        frame_label.result_label_frame(self.result_frame, "頸椎", "檢測結果", "檢測角度", 5, 155, 5, 205, 157, 205)
        frame_label.result_label_frame(self.result_frame, "脊椎", "檢測結果", "檢測角度", 340, 155, 340, 205, 492, 205)
        #下方評論區
        self.text_question = tk.Text(self.question_frame, bg = '#333e50', fg = '#FFFFFF')
        self.text_question.place(x = 5, y = 5)
        self.text_question.tag_configure("center", justify='center')
        self.text_question.insert('end', '問題區', 'center')
        
        self.text_doctor = tk.Text(self.doctor_frame,bg = '#333e50', fg = '#FFFFFF')
        self.text_doctor.place(x = 5, y = 5, width = 650)
        self.text_doctor.tag_configure("center", justify='center')
        self.text_doctor.insert('end', '醫生紀錄填寫區塊', 'center')
    def __init__(self, window = None):
        #變數檢查區塊
        self.x, self.y, self.z = "", "", ""
        self.ID, self.patient_name, self.history_time = "", "", ""
        self.frame_dit, self.frame1_dit = None, None
        self.cap, self.cap1, self.img, self.img1, self.img_detect, self.img1_detect, self.img_path, self.img1_path= None, None, None, None, None, None, None, None
        self.img_ch, self.img1_ch = False, False
        self.picture_path = ""
        self.path1, self.path2 = "", ""
        self.save1_path, self.save2_path = "", ""
        self.pose_key, self.pose1_key = np.array([]), np.array([])
        self.flag, self.flag1 = 1, 1
        self.video_size, self.video1_size = 0, 0
        self.start_ch, self.start_ch1 = False, False
        self.patient_data = []
        # 視窗大小
        self.window = window
        self.window.geometry('1200x850')
        self.window.configure(bg = '#222a35')
        self.window.resizable(width=0, height=0)
        #---------------------------主頁面分類-----------------------------------
        self.tabControl = ttk.Notebook(self.window)          
        self.tab1 = tk.Frame(self.tabControl, bg='#222a35')             
        self.tabControl.add(self.tab1, text='病患資料新增')      
        self.tab2 = tk.Frame(self.tabControl, bg='#222a35')            
        self.tabControl.add(self.tab2, text='病患基本資料')      
        self.tab3 = tk.Frame(self.tabControl, bg='#222a35')            
        self.tabControl.add(self.tab3, text='檢測結果', state = "disabled")      
        self.tab4 = tk.Frame(self.tabControl, bg='#222a35')     
        self.tabControl.pack(expand=1, fill="both")
        #-------------------------------------------------------------------------
        self.add_patient(self.tab1)
        self.patient_history(self.tab2)
        self.detection_page(self.tab3)
    def postData(self, select):
        if not os.path.exists(f"./picture/{self.ID}"):
            os.makedirs(f"./picture/{self.ID}")
        if not os.path.exists(f"./result/{self.ID}"):
            os.makedirs(f"./result/{self.ID}")
        r = requests.post("http://172.20.10.14:8927/takePicture")
        data = r.json()
        data_json = {'type': "tf-pose", "people" : [{"pose_keypoints_2d": np.array(data['kp']).reshape(75).tolist()}]}
        with open(f"./picture/{self.ID}/{data['timestamp']}.json", 'w') as outfile:
            json.dump(data_json, outfile)
        res = requests.get(f"http://172.20.10.14:8927/picture/{data['timestamp']}", stream=True)
        image = np.asarray(bytearray(res.raw.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        cv2.imwrite(f"./picture/{self.ID}/{data['timestamp']}.jpg" ,image)
        self.takepicture_show(select, f"./picture/{self.ID}/{data['timestamp']}.jpg")
        requests.post(f"http://172.20.10.14:8927/picture/{data['timestamp']}")

    #開始檢測
    def start_test(self):
        self.save1_path = self.save2_path = True
        self.save_button['state'] = tk.NORMAL
        self.model_button['state'] = tk.NORMAL
        self.test_results()
    def draw_human_point(self, img, body25):
        for i in range(0, 19):
            if (int(body25[0, i, 0]) != 0 and int(body25[0, i, 1]) != 0):
                img = cv2.circle(
                    img, (int(body25[0, i, 0]), int(body25[0, i, 1])), 10, (255, 0, 0), thickness=-1, lineType=8, shift=0
                    )
        return img
    def draw_human_line(self, img, body25):
        print(body25)
        for i in range(0,18):
            if (int(body25[0, human_line[i][0], 0]) != 0 and int(body25[0, human_line[i][0], 1]) != 0 and int(body25[0, human_line[i][1], 0]) != 0 and int(body25[0, human_line[i][1], 1]) != 0):
                img = cv2.line(img, (int(body25[0,human_line[i][0],0]), int(body25[0,human_line[i][0],1])), (int(body25[0,human_line[i][1],0]), int(body25[0,human_line[i][1],1])), (0, 255, 0), 5)
        return img
    def img_alpha(self, img):
        output = np.zeros((img.shape[0], img.shape[1],img.shape[2]), dtype="uint8")*255
        output.fill(255)

        alpha=0.7 #值越接近0,越透明

        cv2.addWeighted(img, alpha, output, 1 - alpha,0, output)
        return output
    def draw_line(self, img, theta, Left, Right):
        text = "theta = "

        cv2.line(img,(int(Right[0]),int(Right[1])),(int(Left[0]),int(Left[1])),(0,123,255),2)
        cv2.line(img,(int(Right[0]),int(Right[1])),(int(Left[0]),int(Right[1])),(0,155,255),2)
        cv2.putText(img, text+theta, (int(Left[0]) + 10,int(Right[1])), cv2.FONT_HERSHEY_SIMPLEX,0.5, (0, 0, 0), 1, cv2.LINE_8)
        return img
    #計算結果 Nose, Neck, RShoulder, LShoulder, MidHip, Reye, LEye, LEar = 0, 1, 2, 5, 8, 15, 16, 18
    def test_results(self):
        absolute_hand, relatively_hand, absolute_shoulder, absolute_spine, relatively_spine, absolute_c_spine, relatively_c_spine = "", "", "", "", "", "", ""
        self.pose_key[0, MidHip, 0], self.pose_key[0, MidHip, 1] = (self.pose_key[0, 9, 0] + self.pose_key[0, 12, 0] ) / 2 , (self.pose_key[0, 9, 1] + self.pose_key[0, 12, 1] ) / 2
        self.pose1_key[0, MidHip, 0], self.pose1_key[0, MidHip, 1] = (self.pose1_key[0, 9, 0] + self.pose1_key[0, 12, 0] ) / 2 , (self.pose1_key[0, 9, 1] + self.pose1_key[0, 12, 1] ) / 2
        if (len(self.patient_data) == 0):
            self.patient_data.append(self.ID)
        try:
            #正身頭水平 左減右
            print("正身鼻子" + str(self.pose_key[0,Nose,0:2]))
            print("正身頸部" + str(self.pose_key[0,Neck,0:2]))
            print("正身右肩" + str(self.pose_key[0,RShoulder,0:2]), "正身左肩" + str(self.pose_key[0,LShoulder,0:2]))
            print("正身中臀" + str(self.pose_key[0,MidHip,0:2]))
            print("正身右眼" + str(self.pose_key[0,Reye,0:2]), "正身左眼" + str(self.pose_key[0,LEye,0:2]))
            if (self.pose_key[0,LEye,1] < self.pose_key[0,Reye,1]):
                absolute_hand = str(int(math_estimation.arccos_angle(self.pose_key[0,Reye,0:2], self.pose_key[0,LEye,0:2])))
            else:
                absolute_hand = "-" + str(int(math_estimation.arccos_angle(self.pose_key[0,Reye,0:2], self.pose_key[0,LEye,0:2])))
                print(self.pose_key[0,Reye,0:2])
            print(123)
            relatively_hand = str(int(math_estimation.arccos_angle(self.pose_key[0,Reye,0:2], self.pose_key[0,LEye,0:2]) - math_estimation.arccos_angle(self.pose_key[0,RShoulder,0:2], self.pose_key[0,LShoulder,0:2])))
            
            frame_label.result_label_frame(self.result_frame, "頭水平", "相對水平軸" + "\n" + absolute_hand + "度", "相對肩偏移" + "\n" + relatively_hand + "度", 5, 10, 5, 60, 157, 60)
            #正面肩膀
            if (self.pose_key[0,LShoulder,1] < self.pose_key[0,RShoulder,1]):
                absolute_shoulder = str(int(math_estimation.arccos_angle(self.pose_key[0,RShoulder,0:2], self.pose_key[0,LShoulder,0:2])))
            else:
                absolute_shoulder = "-" + str(int(math_estimation.arccos_angle(self.pose_key[0,RShoulder,0:2], self.pose_key[0,LShoulder,0:2])))
            frame_label.result_label_frame1(self.result_frame, "肩膀", "相對水平軸" + absolute_shoulder + "度", 340, 10, 340, 60)
            print(145)
            #正面脊椎
            v1 = math_estimation.point_to_vector(self.pose_key[0,MidHip,0:2], self.pose_key[0,Neck,0:2])
            v2 = math_estimation.point_to_vector(self.pose_key[0,Neck,0:2], self.pose_key[0,Nose,0:2])
            absolute_spine = str(int( self.pose_key[0,Neck,0] - self.pose_key[0,MidHip,0]))
            if (int(absolute_spine) > 0):
                relatively_spine = "-" + str(int(math_estimation.est_angle(v1,v2)))
            else:
                relatively_spine = str(int(math_estimation.est_angle(v1,v2)))
            frame_label.result_label_frame(self.result_frame, "脊椎", "左傾" + absolute_spine  +"單位", "脊椎頭" + "\n" + "連線" + relatively_spine + "度", 340, 155, 340, 205, 492, 205)
        except:
            pass
        try:     
            #側面頸椎
            print("")
            print("側身頸部" + str(self.pose1_key[0,Neck,0:2]))
            print(self.pose1_key[0,LShoulder,0])
            print(self.pose1_key[0,MidHip,0:2])
            print(self.pose1_key[0,LEye,0:2])
            print(self.pose1_key[0,LEar,0])
            v1 = math_estimation.point_to_vector(self.pose1_key[0,MidHip,0:2], self.pose1_key[0,Neck,0:2])
            v2 = math_estimation.point_to_vector(self.pose1_key[0,Neck,0:2], self.pose1_key[0,LEye,0:2])
            absolute_c_spine = str( int(self.pose1_key[0,LShoulder,0] - self.pose1_key[0,LEar,0]) )
            relatively_c_spine = str(int(math_estimation.est_angle(v1,v2)))
            if (int(absolute_c_spine) < 0):
                relatively_c_spine = "-" + str(int(math_estimation.est_angle(v1,v2)))
            else:
                relatively_c_spine = str(int(math_estimation.est_angle(v1,v2)))
            frame_label.result_label_frame(self.result_frame, "頸椎", "前移" + absolute_c_spine + "單位", "前傾" + relatively_c_spine + "度", 5, 155, 5, 205, 157, 205)
        except:
            pass
        s = self.pose_key[:, :,0].astype(str).tolist()
        self.x = ",".join(s[0])
        s = self.pose1_key[:, :,0].astype(str).tolist()
        self.y = ",".join(s[0])
        s = self.pose_key[:, :,1].astype(str).tolist()
        self.z = ",".join(s[0])
        if (len(self.patient_data) == 1):
            self.patient_data.append(self.x)
            self.patient_data.append(self.y)
            self.patient_data.append(self.z)
            self.patient_data.append(absolute_hand)
            self.patient_data.append(relatively_hand)
            self.patient_data.append(absolute_shoulder)
            self.patient_data.append(absolute_spine)
            self.patient_data.append(relatively_spine)
            self.patient_data.append(absolute_c_spine)
            self.patient_data.append(relatively_c_spine)
        else:
            del self.patient_data[1:11]
            self.patient_data.append(self.x)
            self.patient_data.append(self.y)
            self.patient_data.append(self.z)
            self.patient_data.append(absolute_hand)
            self.patient_data.append(relatively_hand)
            self.patient_data.append(absolute_shoulder)
            self.patient_data.append(absolute_spine)
            self.patient_data.append(relatively_spine)
            self.patient_data.append(absolute_c_spine)
            self.patient_data.append(relatively_c_spine)
        if (self.img.all() != None and  self.img_ch == True):
            self.img_detect = self.draw_human_point(self.img, self.pose_key)
            self.img_detect = self.draw_human_line(self.img_detect, self.pose_key)
            self.img_detect = self.img_alpha(self.img_detect)
            self.img_detect = self.draw_line(self.img_detect, absolute_hand, self.pose_key[0,LEye,0:2], self.pose_key[0,Reye,0:2])
            self.img_detect = self.draw_line(self.img_detect, absolute_shoulder, self.pose_key[0,LShoulder,0:2], self.pose_key[0,RShoulder,0:2])
            img = self.img_detect
            img_pose = cv2.resize(self.img_detect, (530, 315))
            img_pose = ImageTk.PhotoImage(Image.fromarray(img_pose))
            self.image_canvas.create_image(0,0,anchor = NW, image = img_pose)
            self.image_canvas.img = img_pose
        if (self.img1.all() != None and  self.img1_ch == True):
            self.img1_detect = self.draw_human_point(self.img1, self.pose1_key)
            self.img1_detect = self.draw_human_line(self.img1_detect, self.pose1_key)
            img1 = self.img1_detect
            img_pose1 = cv2.resize(self.img1_detect, (530, 315))
            img_pose1 = ImageTk.PhotoImage(Image.fromarray(img_pose1))
            self.image1_canvas.create_image(0,0,anchor = NW, image = img_pose1)
            self.image1_canvas.img = img_pose1
        cv2.imwrite(f"./result/{self.ID}/{self.img_path.split('/')[-1].split('.')[0]}.jpg", cv2.cvtColor(self.img_detect, cv2.COLOR_BGR2RGB))
        self.save1_path = f"./result/{self.ID}/{self.img_path.split('/')[-1].split('.')[0]}.jpg"
        cv2.imwrite(f"./result/{self.ID}/{self.img1_path.split('/')[-1].split('.')[0]}.jpg", cv2.cvtColor(self.img1_detect, cv2.COLOR_BGR2RGB))
        self.save2_path = f"./result/{self.ID}/{self.img1_path.split('/')[-1].split('.')[0]}.jpg"
        '''
        t = time.time()
        ti = time.localtime(t)
        s_time = str(time.mktime(ti))
        shot_img = Image.fromarray(img)
        if (not os.path.isdir(self.ID)):
            os.mkdir(self.ID)
        shot_img.save('./AIGO_IFLOW_result/' + self.ID + "/" + str(s_time) + '_1' + '.png', 'PNG')
        self.save1_path = './AIGO_IFLOW_result/' + self.ID + "/" + str(s_time) + '_1' + '.png'
        shot_img = Image.fromarray(img1)
        shot_img.save('./AIGO_IFLOW_result/' + self.ID + "/" + str(s_time) + '_2' + '.png', 'PNG')
        self.save2_path = './AIGO_IFLOW_result/' + self.ID + "/" + str(s_time) + '_2' + '.png'
        '''
    #開始功能
    def start_real_time(self, select):
        if (select == 1):
            self.stat1_button['state'] = tk.DISABLED
            self.stop1_button['state'] = tk.NORMAL
            self.flag = 0
            self.start_ch = False
            if (self.path1 != ""):
                self.video_process_funtion()
        else:
            self.flag1 = 0
            self.start_ch1 = False
            self.stat2_button['state'] = tk.DISABLED
            self.stop2_button['state'] = tk.NORMAL
            if (self.path2 != ""):
                self.video1_process_funtion()
    #停止功能
    def stop_real_time(self, select):
        if (select == 1):
            self.stat1_button['state'] = tk.NORMAL
            self.stop1_button['state'] = tk.DISABLED
            self.start_ch = True
            self.flag = 1
        else:
            self.stat2_button['state'] = tk.NORMAL
            self.stop2_button['state'] = tk.DISABLED
            self.start_ch1 = True
            self.flag1 = 1
    #顯示圖片
    def image_process_funtion(self):
        if (self.img_ch != False):
            img_re = cv2.resize(self.img, (530, 315))
            img_show = ImageTk.PhotoImage(Image.fromarray(img_re))
            self.image_canvas.create_image(0,0,anchor = NW, image = img_show)
            self.image_canvas.img = img_show
        if (self.img1_ch != False):
            img1_re = cv2.resize(self.img1, (530, 315))
            img1_show = ImageTk.PhotoImage(Image.fromarray(img1_re))
            self.image1_canvas.create_image(0,0,anchor = NW, image = img1_show)
            self.image1_canvas.img = img1_show
    #顯示影片
    def video_process_funtion(self):
        ret = False
        video_frame= None
        ret, frame = self.cap.read()
        if (self.flag == 0):
            if (ret != False and self.flag == 0):
                video_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                self.img = video_frame
                video_re = cv2.resize(video_frame, (530,315))
                img_frame = ImageTk.PhotoImage(Image.fromarray(video_re))
                self.image_canvas.create_image(0,0,anchor = NW, image = img_frame)
                self.image_canvas.img = img_frame
            if (self.flag == 0):
                self.image_canvas.after(30, lambda : self.video_process_funtion() )
        self.start_result_state()

    def video1_process_funtion(self):
        ret = False
        video_frame = None
        ret, frame = self.cap1.read()
        if (self.flag1 == 0):
            if (ret == True):
                video_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                self.img1 = video_frame
                video_re = cv2.resize(video_frame, (530,315))
                img_frame = ImageTk.PhotoImage(Image.fromarray(video_re))
                self.image1_canvas.create_image(0,0,anchor = NW, image = img_frame)
                self.image1_canvas.img = img_frame
            if (self.flag1 == 0):
                self.image1_canvas.after(30, lambda : self.video1_process_funtion() )
        self.start_result_state()

    #顯示即時影像
    def real_process_funtion(self):
        while(True):
            ret, frame = self.cap.read()
            ret1, frame1 = self.cap1.read()
            if (ret == True and ret1 == True):
                if (self.flag == 0):
                    self.image_canvas.delete()
                    video_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    self.img = video_frame
                    video_re = cv2.resize(video_frame, (530,315))
                    img_frame = ImageTk.PhotoImage(Image.fromarray(video_re))
                    self.image_canvas.create_image(0,0,anchor = NW, image = img_frame)
                    self.image_canvas.img = img_frame
                    self.image_canvas.update_idletasks()
                    self.image_canvas.update()
                elif(self.flag == 1):
                    self.image_canvas.update_idletasks()
                    self.image_canvas.update()
                if (self.flag1 == 0):
                    self.image1_canvas.delete()
                    video_frame1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2RGB)
                    self.img1 = video_frame1
                    video_re1 = cv2.resize(video_frame1, (530,315))
                    img_frame1 = ImageTk.PhotoImage(Image.fromarray(video_re1))
                    self.image1_canvas.create_image(0,0,anchor = NW, image = img_frame1)
                    self.image1_canvas.img = img_frame1
                    self.image1_canvas.update_idletasks()
                    self.image1_canvas.update()
                elif(self.flag1 == 1):
                    self.image1_canvas.update_idletasks()
                    self.image1_canvas.update()
                self.start_result_state()
            else:
                break

    #real time 功能
    def real_time_button(self):
        self.cap = cv2.VideoCapture(0)
        self.cap1 = cv2.VideoCapture(1)
        self.state_start()
        self.state1_start()
        #self.screenshot_state()
        #self.screenshot1_state()
        self.image_canvas.delete()
        self.image1_canvas.delete()
        self.real_process_funtion()
    #關閉程式
    def button_end(self):
        print(self.patient_data)
        self.window.destroy()
    def takepicture_show(self, select, img_path):
        try:
            if (select == 1):
                self.image_canvas.delete()
                self.img_path = img_path
                if (self.cap != None):
                    self.cap.release()
                    #self.screenshot_state()
                self.stat1_button['state'] = tk.DISABLED
                self.stop1_button['state'] = tk.DISABLED
                path = f"{self.img_path.split('/')[-1].split('.')[0]}.json"
                with open(f'./picture/{self.ID}/{path}' , 'r') as reader:
                    s = json.loads(reader.read())
                    self.pose_key = np.array(s['people'][0]['pose_keypoints_2d']).reshape(1,25,3)
                print(self.pose_key)
                    
                self.start_ch = True
                self.img_ch = True
                self.img = cv2.cvtColor(cv2.imread(self.img_path), cv2.COLOR_BGR2RGB)
                self.image_process_funtion()

            else:
                self.image1_canvas.delete()              
                self.img1_path = img_path
                if (self.cap1 != None):
                    self.cap1.release()
                    #self.screenshot1_state()
                self.stat2_button['state'] = tk.DISABLED
                self.stop2_button['state'] = tk.DISABLED
                path = f"{self.img1_path.split('/')[-1].split('.')[0]}.json"
                with open(f'./picture/{self.ID}/{path}' , 'r') as reader:
                    s = json.loads(reader.read())
                    self.pose1_key = np.array(s['people'][0]['pose_keypoints_2d']).reshape(1,25,3)
                self.start_ch1 = True
                self.img1_ch = True
                self.img1 = cv2.cvtColor(cv2.imread(self.img1_path), cv2.COLOR_BGR2RGB)
                #self.img1 = cv2.flip(cv2.transpose(cv2.cvtColor(cv2.imread(img1_path), cv2.COLOR_BGR2RGB)), 0)
                self.image_process_funtion()

            self.start_result_state()
        except Exception as e:
            print(e)
    #圖片讀入按鈕功能
    def image_button_funtion(self, select):
        try:
            if (select == 1):
                self.image_canvas.delete()
                self.img_path = filedialog.askopenfilename(initialdir = "./",title = "Select file",filetypes = (("jpeg files","*.jpg"), ("png files","*.png"), ("gif files","*.gif"), ("all files","*.*")))
                if (self.img_path.split(".")[-1].lower() in ['bmp', 'jpg', 'jpeg', 'gif', 'png', 'tiff', 'svg']):
                    if (self.cap != None):
                        self.cap.release()
                    #self.screenshot_state()
                    self.stat1_button['state'] = tk.DISABLED
                    self.stop1_button['state'] = tk.DISABLED
                    path = f"{self.img_path.split('/')[-1].split('.')[0]}.json"
                    with open(f'./json/{path}' , 'r') as reader:
                        s = json.loads(reader.read())
                        self.pose_key = np.array(s['people'][0]['pose_keypoints_2d']).reshape(1,25,3)
                    print(self.pose_key)
                    
                    self.start_ch = True
                    self.img_ch = True
                    self.img = cv2.cvtColor(cv2.imread(self.img_path), cv2.COLOR_BGR2RGB)
                    self.image_process_funtion()
                else:
                    self.ErrorMsg()
            else:
                self.image1_canvas.delete()              
                self.img1_path = filedialog.askopenfilename(initialdir = "./",title = "Select file",filetypes = (("jpeg files","*.jpg"), ("png files","*.png"), ("gif files","*.gif"), ("all files","*.*")))
                if (self.img1_path.split(".")[-1].lower() in ['bmp', 'jpg', 'jpeg', 'gif', 'png', 'tiff', 'svg']):
                    if (self.cap1 != None):
                        self.cap1.release()
                    #self.screenshot1_state()
                    self.stat2_button['state'] = tk.DISABLED
                    self.stop2_button['state'] = tk.DISABLED
                    path = f"{self.img1_path.split('/')[-1].split('.')[0]}.json"
                    with open(f'./json/{path}' , 'r') as reader:
                        s = json.loads(reader.read())
                        self.pose1_key = np.array(s['people'][0]['pose_keypoints_2d']).reshape(1,25,3)
                    self.start_ch1 = True
                    self.img1_ch = True
                    self.img1 = cv2.cvtColor(cv2.imread(self.img1_path), cv2.COLOR_BGR2RGB)
                    #self.img1 = cv2.flip(cv2.transpose(cv2.cvtColor(cv2.imread(img1_path), cv2.COLOR_BGR2RGB)), 0)
                    self.image_process_funtion()
                else:
                    self.ErrorMsg()
            self.start_result_state()
        except Exception as e:
            print(e)
    #video fuction
    def video_path(self, select):
        try:
            if (select == 1):
                self.image_canvas.delete()
                video_path = filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("all files","*.*"), ("MP4 files","*.mp4 *.m4v"), ("gif files","*.gif"), ("AVI files","*.avi")))
                if (video_path.split(".")[-1].lower() in ['wmv', 'avi', 'rm', 'mov', 'mpeg', 'asf', 'rmvb', 'flv', 'mp4', 'mjpeg']):
                    self.path1 = video_path
                    if (os.path.getsize(video_path) / 1000 ** 2 < 50):
                        self.cap = cv2.VideoCapture(video_path)
                        #self.screenshot_state()
                        self.state_start()
                        self.video_process_funtion()
                    else:
                        self.SizeError()                   
                else:
                    self.ErrorMsg()

            else:
                self.image1_canvas.delete()
                video1_path = filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("all files","*.*"), ("MP4 files","*.mp4 *.m4v"), ("gif files","*.gif"), ("AVI files","*.avi")))
                if (video1_path.split(".")[-1].lower() in ['wmv', 'avi', 'rm', 'mov', 'mpeg', 'asf', 'rmvb', 'flv', 'mp4', 'mjpeg']):
                    self.path2 = video1_path
                    if (os.path.getsize(video1_path) / 1000 ** 2 < 50):
                        #self.screenshot1_state()
                        self.cap1 = cv2.VideoCapture(video1_path)
                        self.state1_start()
                        self.video1_process_funtion()
                    else:
                        self.SizeError()
                else:
                    self.ErrorMsg()
        except Exception as e:
            print(e)
    #SSH
    def show_3D(self):
        fullbody3D.detection(self.x, self.y, self.z)

    def save_result(self):        
        if (len(self.patient_data) == 15):
            del self.patient_data[11:15]
        self.patient_data.append(self.text_doctor.get("1.0", "end"))
        self.patient_data.append(self.text_question.get("1.0", "end"))
        self.patient_data.append(self.save1_path)
        self.patient_data.append(self.save2_path)
        print(self.patient_data)
        sql.insert_problem(self.patient_data)
        self.patient_data = []
    def ErrorMsg(self):
        messagebox.showinfo("錯誤", "輸入檔案不符合格式")
    def SizeError(self):
        messagebox.showinfo("錯誤", "檔案過大")
    def start_result_state(self):
        if (self.start_ch == True and self.start_ch1 == True):
            self.stat_button['state'] = tk.NORMAL
        else:
            self.stat_button['state'] = tk.DISABLED
        
    def state_start(self):
        if (self.cap != None):
            self.stat1_button['state'] = tk.NORMAL
            self.stop1_button['state'] = tk.DISABLED
        else:
            self.stat1_button['state'] = tk.DISABLED
    def state1_start(self):
        if (self.cap1 != None):
            self.stat2_button['state'] = tk.NORMAL
            self.stop2_button['state'] = tk.DISABLED
        else:
            self.stat2_button['state'] = tk.DISABLED
    def real_time_state(self):
        self.stat1_button['state'] = tk.NORMAL
        self.stat2_button['state'] = tk.NORMAL
    '''
    def screenshot_state(self):
        self.screenshot1_button['state'] = tk.DISABLED
    def screenshot1_state(self):
        self.screenshot2_button['state'] = tk.DISABLED
    '''
    #---------------新增資料到列表-------------------------------------------
    def add_data(self): 
        self.update_patient_button['state'] = DISABLED
        if (self.IDnumber_entry.get() != "" and self.name_entry.get() != "" and self.combo.get() != "" and self.height_entry.get() != "" and self.weight_entry.get() != ""):
            if (self.checkID(self.IDnumber_entry.get())):
                if (10 < int(self.height_entry.get()) and 300 > int(self.height_entry.get())):
                    if (10 < int(self.weight_entry.get()) and 200 > int(self.weight_entry.get())):
                        self.aList = [self.IDnumber_entry.get(),self.name_entry.get(), self.combo.get(), self.height_entry.get(), self.weight_entry.get()]
                        self.aTuple = tuple(self.aList)
                        sql.insert_basic(self.aTuple) 
                        self.IDnumber_entry.delete(0,END)
                        self.name_entry.delete(0,END)
                        self.combo.set('')
                        self.height_entry.delete(0,END)
                        self.weight_entry.delete(0,END)
                    else:
                        messagebox.showerror("錯誤", "重量輸入格式不符合")
                else:
                    messagebox.showerror("錯誤", "身高輸入格式不符合")
            else :
                messagebox.showerror("錯誤", "身份證號碼輸入格式不符合")
        else:
            messagebox.showerror("錯誤", "資料請勿空白")
    ####變數名稱要改
    def delete_data(self):
        for item in self.tree.selection():
            item_text = self.tree.item(item,"values")
         
        self.A = sql.select_basic(item_text[0])
        self.A = list(self.A)
        sql.delete_basic(str(self.A[0][1]))
        self.search_data() 
        
        self.A2 = sql.select_problem(item_text[0])
        self.A2 = list(self.A2)
        for delete_all_data in self.A2:
            sql.delete_basic(str(self.A2[0][2]))
        self.search_data()
    
    def search_data(self):
        self.delete_button['state'] = DISABLED
        delete_tree=self.tree.get_children() #刪除tree列表
        for item in delete_tree:
            self.tree.delete(item)
            
        delete_tree2=self.tree2.get_children() #刪除tree2列表
        for item in delete_tree2:
            self.tree2.delete(item)
            
        if (sql.select_basic(self.id_entry.get())): #sele查詢
            self.E = sql.select_basic(self.id_entry.get())
            for people_data in self.E:
                self.tree.insert('', 'end', values = people_data[1:]) #新增資料到tree
        
            if (sql.select_problem(self.id_entry.get())):
                self.E2 = sql.select_problem(self.id_entry.get())
                for time_data in self.E2:
                    self.tree2.insert('', 'end', values = time_data[13:15])
            else:
                pass     
        else:
            pass
    
    def modify_data(self):
        #修改新視窗
        self.new_update_window = tk.Tk()
        self.new_update_window.title("修改資料")
        self.new_update_window.geometry('800x650')
        self.new_update_window.configure(bg = '#222a35')

        IDnumber_label = tk.Label(self.new_update_window, text='身份證字號：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        IDnumber_label.place(x = 50, y = 50)
        name_label = tk.Label(self.new_update_window, text='姓名：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        name_label.place(x = 160, y = 150)
        gender_label = tk.Label(self.new_update_window, text='性別：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        gender_label.place(x = 160, y = 250)
        height_label = tk.Label(self.new_update_window, text='身高：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        height_label.place(x = 160, y = 350)
        weight_label = tk.Label(self.new_update_window, text='體重：',# 標籤的文字
                               bg='#222a35',     # 背景颜色
                               font=('微軟正黑體', 28, 'bold'),     # 字體和字體大小
                               fg="#FFFFFF"
                              )
        weight_label.place(x = 160, y = 450)

        self.ID_update_entry = Entry(self.new_update_window, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.ID_update_entry.place(x = 300, y = 50, width=350, height=50)

        self.name_update_entry = Entry(self.new_update_window, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.name_update_entry.place(x = 300, y = 150, width=350, height=50)

        self.combo_update = ttk.Combobox(self.new_update_window, value = ["男","女"], font=('微軟正黑體', 25, 'bold'))
        self.combo_update.place(x = 300, y = 250, width=350, height=50)
        self.combo_update['state'] = 'readonly'

        self.height_update_entry = Entry(self.new_update_window, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.height_update_entry.place(x = 300, y = 350, width=350, height=50)

        self.weight_update_entry = Entry(self.new_update_window, textvariable = tk.StringVar(), bd=3, fg="#474747", highlightcolor='red', highlightthickness=1, font=('微軟正黑體', 28, 'bold'))
        self.weight_update_entry.place(x = 300, y = 450, width=350, height=50)

        self.update_button = tk.Button(self.new_update_window,text='修改資料',font=('微軟正黑體', 20, 'bold'), bg = '#8397b0', fg="#FFFFFF" ,command= self.update_patient_data)
        self.update_button.place(x = 400, y = 550)

        for item in self.tree.selection():
            item_text = self.tree.item(item,"values")
            self.ID_update_entry.insert(0,item_text[0])#抓取
            self.ID_update_entry.config(state='disabled')
            self.name_update_entry.insert(0,item_text[1])
            self.height_update_entry.insert(0,item_text[3])
            self.weight_update_entry.insert(0,item_text[4])
        if (item_text[2] == "男"):
            self.combo_update.current(0)
        else:
            self.combo_update.current(1)
        
    
    def update_patient_data(self):
        self.new_button['state'] = NORMAL
        self.update_button['state'] = DISABLED
        self.update_patient_button['state'] = DISABLED
        self.aList = [self.ID_update_entry.get(),self.name_update_entry.get(), self.combo_update.get(), self.height_update_entry.get(), self.weight_update_entry.get()]
        self.aTuple = tuple(self.aList)
        sql.update(self.aTuple) 
        self.ID_update_entry.config(state='normal')
        self.new_update_window.destroy()
        self.search_data()

    def history_report(self):
        colum_check = 0
        ID = self.id_entry.get()
        b = []
        data = np.array([])
        self.E2 = sql.select_problem(ID)
        for time_data in self.E2:
            a = list(time_data)
            for i in report_column:
                b.append(a[i])
        data = np.array(b)
        print(len(b))
        dataf = pd.DataFrame(data.reshape(len(b) // 13, 13), columns =["時間", "正身x", "正身y", "側身x", "絕對頭水平", "相對頭水平", "絕對肩水平", "絕對脊椎", "相對脊椎", "絕對頸椎", "相對頸椎", "問題", "醫生建議"])

        dataf.to_csv(ID + ".csv", index = False, header = True)
    def checkID(self, id_number):#身分證
       # int_ID = int(self.IDnumber_entry.get())
        self.idchk = "0123456789ABCDEFGHJKLMNPQRSTUVXYWZIO"
        self.pattern = '^[A-Z]{1}(1|2)\\d{8}$'
        self.id_number = id_number.upper()
        if re.match(self.pattern, self.id_number):
  
            n1 = self.idchk.find(self.id_number[0])
            total = int(n1 / 10 + (n1 % 10) * 9)
  
            for x in range(1,9):
                total += self.idchk.find(self.id_number[x]) * (9 - x)
            total += self.idchk.find(self.id_number[9])
            if total % 10 == 0:
                return True
            else:
                return False
        else:
            return False

if __name__ == '__main__':
    if not os.path.exists("./picture"):
        os.makedirs("./picture")
    if not os.path.exists("./result"):
        os.makedirs("./result")
    root = tk.Tk()
    root.title("愛豐健康概念館")
    sql = pose_sql.SQL()
    try:
        sql.create_basic()
    except:
        pass
    try:
        sql.create_problem()
    except:
        pass
    app = App(root)
    app.window.mainloop()
##檢查ENTRY刪除是否有重複 重複改寫成副程式
#A A2 得改名稱
#SQL.py 要改變數
