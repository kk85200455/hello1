import re
from numpy.lib.type_check import imag
from tf_pose import common
from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh
import cv2
import numpy
import json


class TfProcess:
    def __init__(self):
        self.w, self.h = model_wh("432x368")
        self.tf = TfPoseEstimator(get_graph_path("cmu"), target_size=(self.w, self.h))
        print("[TF Pose] init complate")

    def run(self, image):
        detect_res = self.tf.inference(image, resize_to_default=(self.w > 0 and self.h > 0), upsample_size=4.0)
        image, kp = self.key_point_parsing(image, detect_res, imgcopy=False)
        print(kp)
        kp = self.cocoToBody25(kp)
        #image = self.draw(image, kp)
        print("[TF Pose] Done")
        return kp

    def key_point_parsing(self, npimg, detect_res, imgcopy=False):
        if imgcopy:
            npimg = numpy.copy(npimg)
        image_h, image_w = npimg.shape[:2]
        centers = {}
        humansPoints = []
        for h in detect_res:
            points = [[0, 0, 0] for i in range(common.CocoPart.Background.value)]
            # draw point
            for i in range(common.CocoPart.Background.value):
                if i not in h.body_parts.keys():
                    continue

                body_part = h.body_parts[i]
                center = (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
                centers[i] = center

                point = [body_part.x * image_w, body_part.y * image_h, body_part.score]
                points[i] = point

                # cv2.circle(npimg, center, 3, common.CocoColors[i], thickness=3, lineType=8, shift=0)
            humansPoints = [points]

            # draw line
            for pair_order, pair in enumerate(common.CocoPairsRender):
                if pair[0] not in h.body_parts.keys() or pair[1] not in h.body_parts.keys():
                    continue

                # cv2.line(npimg, centers[pair[0]], centers[pair[1]], common.CocoColors[pair_order], 3)
        return npimg, humansPoints

    def cocoToBody25(self, cocoKeypoints):
        cocoKeypoints = numpy.array(cocoKeypoints)
        body25Keypoints = numpy.zeros((1, 25, 3))
        for i in range(0, 8):
            for j in range(0, 2):
                body25Keypoints[0, i, j] = cocoKeypoints[0, i, j]
                
        for i in range(0, 2):
            body25Keypoints[0, 8, i] = cocoKeypoints[0][8][i] + cocoKeypoints[0][11][i]

        for i in range(9, 19):
            for j in range(0, 2):
                body25Keypoints[0, i, j] = cocoKeypoints[0, i - 1, j]
        for i in range(19, 22):
            for j in range(0, 2):
                body25Keypoints[0, i, j] = cocoKeypoints[0, 13, j]
        for i in range(22, 25):
            for j in range(0, 2):
                body25Keypoints[0, i, j] = cocoKeypoints[0, 10, j]

        return body25Keypoints

    def draw(self, npimg, body25):
        for i in range(0, 19):
            cv2.circle(
                npimg, (int(body25[0, i, 0]), int(body25[0, i, 1])), 6, (255, 0, 0), thickness=3, lineType=8, shift=0
            )
            cv2.putText(npimg, str(i), (int(body25[0, i, 0]) + 20, int(body25[0, i, 1])+20), cv2.FONT_HERSHEY_SIMPLEX,0.5, (0, 0, 0), 1, cv2.LINE_8)
        return npimg
